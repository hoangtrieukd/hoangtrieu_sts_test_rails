module Api
  module V1
    class CustomersController < ApiV1Controller
      before_action :set_customer, only: %i[show update destroy]

      def index
        data = Customer.order(params[:order]).page(params[:page]).per(params[:per])
        render_response_success(200, data,
                                {
                                    each_serializer: CustomerSerializer,
                                    total_pages: data.total_pages
                                })
      end

      def show
        render_response_success(200, @customer, { serializer: CustomerSerializer })
      end

      def create
        @customer = Customer.new(customer_params)
        if @customer.save
          render_response_success(200, @customer, { serializer: CustomerSerializer })
        else
          render_response_fail(422, @customer.errors.messages)
        end
      end

      def update
        @customer.assign_attributes(customer_params)
        if @customer.save
          render_response_success(200, @customer, { serializer: CustomerSerializer })
        else
          render_response_fail(422, @customer.errors.messages)
        end
      end

      def destroy
        if @customer.delete
          render_response_success(204)
        end
      end

      private

      def customer_params
        params.require(:customer).permit(:first_name, :last_name, :avatar_url, :email, :phone, :is_active)
      end

      def set_customer
        @customer = Customer.find_by_id(params[:id]) || render_response_not_found(params[:id])
      end
    end
  end
end