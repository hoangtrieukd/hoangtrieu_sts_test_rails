module HttpHelper
  # Success statuses
  # 200 OK
  # 201 Created
  # 202 Accepted
  # 203 Non-Authoritative Information (since HTTP/1.1)
  # 204 No Content
  # 205 Reset Content
  # 206 Partial Content
  # 207 Multi-Status
  # 208 Already Reported
  # 226 IM Used
  def render_response_success(status_code, data = [], options = {})
    if options[:serializer] || options[:each_serializer]
      data = ActiveModelSerializers::SerializableResource.new(data, options)
    end
    if options[:total_pages]
      render json: { status: 'success', data: data, totalPages: options[:total_pages] }, status: status_code
    else
      render json: { status: 'success', data: data }, status: status_code
    end
  end

  # Fail statuses
  # 400 Bad Request
  # 401 Unauthorized
  # 403 Forbidden
  # 405 Method Not Allowed
  # 408 Request Timeout
  # 422 Unprocessable Entity
  # 500 Internal Server Error
  # 502 Bad Gateway
  # 504 Gateway Timeout
  def render_response_fail(status_code, data = [])
    render json: { status: 'fail', errors: data }, status: status_code
  end

  # 404 Not Found
  def render_response_not_found(id)
    render json: { status: 'not_found', errors: "Couldn't find record with 'id'=#{id}" }, status: 404
  end
end
