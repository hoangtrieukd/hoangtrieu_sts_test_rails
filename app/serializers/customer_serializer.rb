class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :last_name, :first_name, :phone, :is_active, :avatar_url, :email
end