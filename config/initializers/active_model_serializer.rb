# Key Transforms modify the casing of keys and keys referenced in values in serialized responses.
ActiveModelSerializers.config.key_transform = :camel_lower
ActiveModelSerializers.config.default_includes = '**'