# README

## Installation

# Steps for setup

## Step 1: Get the rails source from Github repo 
```
git clone git@bitbucket.org:hoangtrieukd/hoangtrieu_sts_test_rails.git

```

## Step 2: Install gems & front-end libraries using `bundle`
```
bundle install

```
## Step 3: Config databases

```
cp config/database.sample.yml config/database.yml
cp config/application.sample.yml config/application.yml
```

## Data for test
```
rake db:seed
```
