# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean

100.times do |num|
  Customer.create({
      last_name: "last name #{num}",
      first_name: "first name #{num}",
      email: "email #{num}",
      phone: "phone #{num}",
                  })
end