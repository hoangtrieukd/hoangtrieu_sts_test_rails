class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :first_name
      t.string :last_name
      t.string :avatar_url
      t.string :email
      t.string :phone
      t.boolean :is_active
    end
  end
end
